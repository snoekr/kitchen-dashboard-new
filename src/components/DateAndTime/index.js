import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import { Heading1, Paragraph } from '../styled-components/'

class DateAndTime extends Component {

  static defaultProps = {
    currentDate: moment().format("ddd DD MMMM"),
    currentTime: moment().format("HH:mm a")
  }

  render() {
    const { currentDate, currentTime } = this.props;
    return (
      <div>
        <Paragraph>{currentDate}</Paragraph>
        <Heading1>{currentTime}</Heading1>
      </div>
    );
  }
}

DateAndTime.propTypes = {
  currentDate: PropTypes.string.isRequired,
  currentTime: PropTypes.string.isRequired,
}

export default DateAndTime;