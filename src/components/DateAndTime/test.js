import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import DateAndTime from './';
import moment from 'moment';

describe('DateAndTime', () => {

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<DateAndTime />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test.skip('has a valid snapshot', () => {
    let props = {
      currentDate: moment('2018-01-01 12:00:00'),
      currentTime: moment('2018-01-01 12:00:00'),
    }

    const component = renderer.create(
      <DateAndTime /> 
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

})

