import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import Kitchen from '../../dashboards/Kitchen'
import Sales from '../../dashboards/Sales'

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" render={() => 
            <div>
              <Link to="/kitchendashboard">Kitchen Dashboard</Link>
              <Link to="/salesdashboard">Sales Dashboard</Link>
            </div>
          }/>
          <Route path="/kitchendashboard" component={Kitchen} />
          <Route path="/salesdashboard" component={Sales} />
        </Switch>
      </Router>
    );
  }
}

export default App;
