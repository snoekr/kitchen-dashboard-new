import styled from 'styled-components';

 const ListItem = styled.li`
  margin: 0;
  font-size: 33px;
  text-transform: uppercase;
  list-style-type: none;
` 

export default ListItem;