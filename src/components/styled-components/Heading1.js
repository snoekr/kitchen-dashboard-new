import styled from 'styled-components';

const Heading1 = styled.h1`
  margin: 0;
  font-size: 33px;
  line-height: 45px;
  text-transform: uppercase;
`

export default Heading1;