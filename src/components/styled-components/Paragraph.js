import styled from 'styled-components';

const Paragraph = styled.p`
  margin: 0;
  font-size: 16px;
  line-height: 22px;
  text-transform: uppercase;
`

export default Paragraph;