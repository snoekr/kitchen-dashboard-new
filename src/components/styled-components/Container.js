import styled from 'styled-components';

const Heading1 = styled.div`
  width: ${props => props.containerWidth}
  height: ${props => props.containerHeight}
  background: rgba(98, 98, 98, 0.5);
  backdrop-filter: blur(40px);
`

export default Heading1