import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import { Heading1, Paragraph, ListItem } from '../styled-components/'

const birthdayPropList = [
    {
        "name": "Rutger",
        "birthdate": "2010-10-22"
    },
    {
        "name": "Burhan",
        "birthdate": "2018-10-24"
    },
    {
        "name": "Leo",
        "birthdate": "2018-10-26"
    },
    {
        "name": "Sophie",
        "birthdate": "2018-11-21"
    },
]

function getBirthdaysForThisWeek(currentDate, date) {
    const newDate = moment(date).year(currentDate.year());
    if (newDate.isoWeek() == currentDate.isoWeek()) return true
    return false
}

const BirthdayList = ({currentDate, listOfBirthdays}) =>
    <div>
        <Heading1>Birthdays</Heading1>
        <Paragraph>This week</Paragraph>
        <ul style={{padding: 0}}>
            { listOfBirthdays.map((birthdayItem, index) => {
                if (getBirthdaysForThisWeek(currentDate, birthdayItem.birthdate)) {
                    return <ListItem key={index}>
                        <span>{birthdayItem.name}</span>
                        <span style={{textAlign: 'right'}}>{moment(birthdayItem.birthdate).format("DD MMM")}</span>
                    </ListItem>
                }
            })}
        </ul>
    </div>

export default BirthdayList;

BirthdayList.defaultProps = {
    currentDate: moment(),
    listOfBirthdays: birthdayPropList
}
