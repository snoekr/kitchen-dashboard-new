import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import BirthdayList from './';
import moment from 'moment';

Enzyme.configure({ adapter: new Adapter() });

const currentDate = moment("2018-10-24");
const birthdayPropList = [
  {
      "name": "Rutger",
      "birthdate": "2010-10-22"
  },
  {
      "name": "Burhan",
      "birthdate": "2018-10-24"
  },
  {
      "name": "Leo",
      "birthdate": "2018-10-26"
  },
  {
      "name": "Sophie",
      "birthdate": "2018-11-21"
  },
]

describe('BirthdayList', () => {

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<BirthdayList />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('expect 3 items in the birthdaylist', () => {
    const props = { currentDate, birthdayPropList };

    const element = shallow(
      <BirthdayList { ...props } />
    );

    expect(element.find('li').length).toBe(3);
  })


  test('has a valid snapshot', () => {
    let props = birthdayPropList;

    const component = renderer.create(
      <BirthdayList /> 
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

})

