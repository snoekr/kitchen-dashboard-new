import React, { Component } from 'react';
import DateAndTime from '../../components/DateAndTime';
import BirthdayList from '../../components/BirthdayList';
import { Container } from '../../components/styled-components'

class Kitchen extends Component {
  render() {
    return (
      <div>
        <Container containerWidth="25%" containerHeight="100%">
          <DateAndTime/>
        </Container>
        <div>
          <BirthdayList/>
        </div>
      </div>
    );
  }
}

export default Kitchen;
