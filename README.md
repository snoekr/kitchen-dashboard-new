## The new Kitchen Dashboard

### Installation

Run `npm i` to install the project dependencies and dev dependencies.

### Starting the app

Run `npm start` to start the application.

> After running the above command the app should be automatically opened in the browser and it should be running on port 3000.

### Automated tests

Run `npm t` to run the automated tests.